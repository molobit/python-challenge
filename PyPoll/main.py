'''
PyPoll
Using the file election_data.csv, this scripts calculates the following:
    * The total number of votes cast
    * A complete list of candidates who received votes
    * The percentage of votes each candidate won
    * The total number of votes each candidate won
    * The winner of the election based on popular vote.
'''
import os
import csv

cols = {
        "VoterId": 0,
        "County": 1,
        "Candidate": 2,
    }

poll_stats = {
        "TotalVotes": 0,
        "Candidates": {},
        "Winner": {"Candidate": "", "Votes": 0},
    }


def load_poll_data():
    election_data = {
        "VoterId": [],
        "County": [],
        "Candidate": []
    }

    csv_file = os.path.join(
            os.path.dirname(__file__),
            'Resources',
            'election_data.csv'
        )
    with open(csv_file, 'r') as handle:
        csv_reader = csv.reader(handle, delimiter=',')

        next(csv_reader)

        for row in csv_reader:
            election_data['VoterId'].append(row[cols['VoterId']])
            election_data['County'].append(row[cols['County']])
            election_data['Candidate'].append(row[cols['Candidate']])

    return election_data


def calculate_poll_stats(election_data):
    # got total votes from election data
    poll_stats['TotalVotes'] = len(election_data['VoterId'])

    for i in range(poll_stats['TotalVotes']):
        candidate = election_data['Candidate'][i]
        if not (candidate in poll_stats['Candidates']):
            poll_stats['Candidates'][candidate] = 1
        else:
            poll_stats['Candidates'][candidate] += 1

    for candidate in poll_stats['Candidates']:
        if poll_stats['Candidates'][candidate] > poll_stats['Winner']['Votes']:
            poll_stats['Winner']['Votes'] = poll_stats['Candidates'][candidate]
            poll_stats['Winner']['Candidate'] = candidate


def format_poll_stats():
    individual_stats_data = []

    for candidate in poll_stats['Candidates']:
        candidate_votes = poll_stats['Candidates'][candidate]
        percent = (candidate_votes / poll_stats['TotalVotes']) * 100.0
        stat = "%s %s (%s)" % \
            (candidate, "%{:.3f}".format(percent), candidate_votes)
        individual_stats_data.append(stat)

    individual_stats = "\n    ".join(individual_stats_data)

    return f'''
                Election Results
--------------------------------------------------------
    Total Votes {poll_stats["TotalVotes"]}
--------------------------------------------------------
    {individual_stats}
--------------------------------------------------------
    Winner: {poll_stats["Winner"]["Candidate"]}
--------------------------------------------------------
            '''


def export_poll_stats(report):
    export_file_path = os.path.dirname(__file__)
    poll_data_export_file = os.path \
        .join(export_file_path, 'Resources', 'poll_data.txt')
    with open(poll_data_export_file, 'w') as handle:
        handle.write(report)


def main():
    poll_data = load_poll_data()
    calculate_poll_stats(poll_data)
    report = format_poll_stats()
    export_poll_stats(report)
    print(report)


if __name__ == '__main__':
    main()
