'''
PyBank
Using the file budget_data.csv, this scripts calculates the following:
    * The total number of months included in the dataset
    * The net total amount of "Profit/Losses" over the entire period
    * Calculate the changes in "Profit/Losses" over the entire period,
        then find the average of those changes
    * The greatest increase in profits (date and amount) over the entire period
    * The greatest decrease in losses (date and amount) over the entire period
'''

import csv
import os
from statistics import geometric_mean, mean
from datetime import datetime

'''
cols variable: references the indexes in the csv structure
'''
cols = {
        "Date": 0,
        "ProfitLosses": 1
    }

bank_stats = {
        "TotalMonths": 0,
        "NetTotal": 0,
        "AvgChange": 0,
        "MaxChange": {"Date": None, "Value": 0},
        "MinChange": {"Date": None, "Value": None},
    }


def calculate_bank_stats(bank_data):
    bank_stats["TotalMonths"] = len(bank_data["Date"])
    bank_stats["NetTotal"] = sum(bank_data["ProfitLosses"])

    changes = []
    for i in range(len(bank_data["ProfitLosses"])):
        next_entry = i + 1
        if (next_entry < len(bank_data["ProfitLosses"])):

            # calcualte average change
            next_profit_loss = bank_data["ProfitLosses"][next_entry]
            current_profit_loss = bank_data["ProfitLosses"][i]
            profit_change = next_profit_loss - current_profit_loss
            changes.append(profit_change)

            # greatest increase in profits
            if (bank_stats["MaxChange"]["Value"] < profit_change):
                bank_stats["MaxChange"]["Value"] = profit_change
                bank_stats["MaxChange"]["Date"] = bank_data["Date"][next_entry]

            # greatest decrease in profits
            if (bank_stats["MinChange"]["Value"] is None or
                    bank_stats["MinChange"]["Value"] > profit_change):
                bank_stats["MinChange"]["Value"] = profit_change
                bank_stats["MinChange"]["Date"] = bank_data["Date"][next_entry]

    bank_stats["AvgChange"] = mean(changes)


def export_bank_stats(bank_data_report):
    bank_data_export_file = os.path.join(
            os.path.dirname(__file__),
            'Resources',
            'bank_data.txt'
        )
    with open(bank_data_export_file, 'w') as handle:
        handle.write(bank_data_report)


def format_bank_stats():
    max_change_date = bank_stats["MaxChange"]["Date"]
    max_change = bank_stats["MaxChange"]["Value"]
    min_change = bank_stats["MinChange"]["Value"]
    min_change_date = bank_stats["MinChange"]["Date"]

    return f'''
                Financial Analysis
--------------------------------------------------------
   Total Months:                 {bank_stats["TotalMonths"]}
   Total:                        ${bank_stats["NetTotal"]:,}
   Average Change:               {bank_stats["AvgChange"]: .2f}
   Greatest Increase In Profits: {max_change_date:%b %Y} ({max_change})
   Greatest Decrease In Profits: {min_change_date:%b %Y} ({min_change})
    '''


def load_bank_data():
    bank_data = {"Date": [], "ProfitLosses": []}

    budget_data_file = os.path.join(
            os.path.dirname(__file__),
            'Resources',
            'budget_data.csv'
        )

    with open(budget_data_file, 'r') as handle:
        csv_reader = csv.reader(handle, delimiter=',')

        '''skip header'''
        next(csv_reader)

        for row in csv_reader:
            date = datetime.strptime(row[cols['Date']], '%b-%Y')
            bank_data["Date"].append(date)
            bank_data["ProfitLosses"].append(int(row[cols['ProfitLosses']]))

    return bank_data


def main():
    bank_data = load_bank_data()
    calculate_bank_stats(bank_data)
    report = format_bank_stats()
    export_bank_stats(report)
    print(report)


if __name__ == '__main__':
    main()
