# Python Challenge #

In this project you will find scripts that will process CSV files for a fictitious
data for an election results and a finanacial institution


# PyBank #

Using the file ```budget_data.csv```, this scripts calculates the following:

* The total number of months included in the dataset
* The net total amount of "Profit/Losses" over the entire period
* Calculate the changes in "Profit/Losses" over the entire period, then find the average of those changes
* The greatest increase in profits (date and amount) over the entire period
* The greatest decrease in losses (date and amount) over the entire period

# PyPoll #

Using the file ```election_data.csv```, the main script will calculate the following:
* The total number of votes cast
* A complete list of candidates who received votes
* The percentage of votes each candidate won
* The total number of votes each candidate won
* The winner of the election based on popular vote.
